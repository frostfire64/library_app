'use strict';
//entry point for my app

const express = require('express')
    ,session = require('express-session')
    ,morgan = require('morgan')
    ,ejs = require('ejs')
    ,frontend = require('./libs/frontend')
    ,bodyParser = require('body-parser');

const app = express();
app.set('trust proxy', 1); // trust first proxy
app.use(morgan('dev'));
app.use(session({
    secret: 'sup3rSeCR3tLIbRArrryApp',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
}));
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));

function controllerLoader(req,res,next) {
    const routes = require('./routes');
    let route = req.method+':'+req.path;
    let classpath;
    try {
        classpath = routes[route].split('@');
    } catch (error) {
        res.status(404).send('Route '+route+' not found');
        return;
    }
    let controller;
    try {
        controller = require('./controllers/'+classpath[0]);
    } catch (error) {
        res.status(404).send('Controller /controllers/'+classpath[0]+'.js not found');
        return;
    }
    let controllerMethod = classpath[1];
    try {
        let func = controller[controllerMethod];
        switch (req.method) {
        case 'GET':
            app.get(req.path,func);
            break;
        case 'POST':
            app.post(req.path,func);
            break;
        default:
            break;
        }
        app.get(req.path,func);
    } catch (error) {
        res.status(404).send(classpath[0]+'.'+controllerMethod+' is not a function');
        return;
    }
    next();
}

app.use(controllerLoader);

//routes

// const users = require('./controllers/users');
// app.use('/users',users);

app.listen(8080,() => {
    console.log('Server started on port 8080');
});