const frontend = require('../libs/frontend')
    ,ejs = require('ejs');

const users = {
    index: (req,res) => {
        if (req.session.login) {
            frontend.renderView('users/index',null,res);
        } else {
            ejs.renderFile('./views/login.ejs',null,null,(err,str)=>{
                res.send(str);
            });
        }
    },
    login: (req,res) => {
        req.session.login = true;
        console.log(req.body);
        // res.send('<pre>'+JSON.stringify(req.body)+'</pre>');
        res.redirect('/');
    },
    logout: (req,res) => {
        req.session.login = false;
        console.log(req);
        res.redirect('/');
    }
};

module.exports = users;