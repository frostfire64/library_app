const ejs = require('ejs');

let frontend = {
    renderView: (view,data,res) => {
        ejs.renderFile('./views/'+view+'.ejs',data,null,(err,str)=>{
            let mainData = {
                content: str
            };
            ejs.renderFile('./views/index.ejs',mainData,null,(err,str)=>{
                res.send(str);
            });
        });
        
    }
};

module.exports = frontend;