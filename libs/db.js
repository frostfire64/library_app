let knex,
    fs = require('fs');

fs.exists('./assets/mydb.sqlite',(exists) => {
    if (exists) {
        console.log('found the db file.');
    } else {
        console.error('can\'t find the db file.');
    }
});

if (typeof knex == 'undefined'){
    let config = require('../knexfile');
    // knex = require('knex')({
    //     client: 'sqlite3',
    //     connection: {
    //         filename: "./assets/mydb.sqlite"
    //     }
    // });
    knex = require('knex')(config);
}

module.exports = knex;